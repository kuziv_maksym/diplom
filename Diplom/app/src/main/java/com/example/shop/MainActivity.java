package com.example.shop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {


    private ImageButton capButton;
    private ImageButton shirtButton;
    private ImageButton trousersButton;
    private ImageButton shoesButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        capButton = (ImageButton) findViewById(R.id.capBtn);
        shirtButton = (ImageButton) findViewById(R.id.shirtBtn);
        trousersButton = (ImageButton) findViewById(R.id.trousersBtn);
        shoesButton = (ImageButton) findViewById(R.id.shoesBtn);
        shoesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startShoe();
                Choise.choise = "Shoes";
            }
        });
        capButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCap();
                Choise.choise = "Cap";
            }
        });
        trousersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTrousers();
                Choise.choise = "Trousers";
            }
        });
        shirtButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startShirt();
                Choise.choise = "Shirt";
            }
        });
    }

    public void startTrousers() {
        Intent intent = new Intent(this, Choise.class);
        startActivity(intent);
    }
    public void startShirt() {
        Intent intent = new Intent(this, Choise.class);
        startActivity(intent);
    }

    public void startShoe() {
        Intent intent = new Intent(this, Choise.class);
        startActivity(intent);
    }
    public void startCap() {
        Intent intent = new Intent(this, Choise.class);
        startActivity(intent);
    }


}
