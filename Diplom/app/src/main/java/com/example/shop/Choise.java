package com.example.shop;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Choise extends AppCompatActivity {

    public static String choise;
    String nike;
    String puma;
    String addidas;
    String trousers;
    Button buttonNike;
    Button buttonPuma;
    Button buttonAddidas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choise);

        switch (choise) {
            case "Cap":
                nike = "NikeCap";
                puma = "PumaCap";
                addidas = "AddidasCap";
                trousers = "";
                break;
            case "Shoes":
                nike = "";
                puma = "PumaShoe";
                addidas = "AddidasShoe";
                trousers = "";
                break;
            case "Trousers":
                nike = "NikeTr";
                puma = "";
                addidas = "";
                trousers = "";
                break;
            case "Shirt":
                nike = "";
                puma = "";
                addidas = "AddidasShirt";
                trousers = "";
                break;
        }
        buttonNike = (Button) findViewById(R.id.NikeBtn);
        buttonPuma = (Button) findViewById(R.id.PumaBtn);
        buttonAddidas = (Button) findViewById(R.id.AddidasBtn);
        buttonPuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPuma();
                Cap.db = puma;
            }
        });
        buttonAddidas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddidas();
                Cap.db = addidas;
            }
        });
        buttonNike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNike();
                Cap.db = nike;
            }
        });
    }

    public void openNike() {
        Intent intent = new Intent(this, Cap.class);
        startActivity(intent);
    }

    public void openPuma() {
        Intent intent = new Intent(this, Cap.class);
        startActivity(intent);
    }

    public void openAddidas() {
        Intent intent = new Intent(this, Cap.class);
        startActivity(intent);
    }

}
