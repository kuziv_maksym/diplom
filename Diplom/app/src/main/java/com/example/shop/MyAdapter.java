package com.example.shop;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    DatabaseReference databaseReference;
    Order order;
    Context context;
    ArrayList<Model> models;
    public MyAdapter(Context c, ArrayList<Model> m){
        context = c;
        models = m;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.name.setText(models.get(position).getName());
        holder.price.setText(models.get(position).getPrice());
        holder.description.setText(models.get(position).getDescription());
        Picasso.get().load(models.get(position).getImage()).into(holder.imageView);
        holder.btn.setVisibility(View.VISIBLE);
        holder.onClick(position);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView name;
        TextView price;
        TextView description;
        ImageView imageView;
        Button btn;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            price = (TextView) itemView.findViewById(R.id.price);
            description = (TextView) itemView.findViewById(R.id.description);
            imageView = (ImageView) itemView.findViewById(R.id.modelPicture);
            btn = (Button) itemView.findViewById(R.id.checkProduct);
            order = new Order();
            databaseReference = FirebaseDatabase.getInstance().getReference().child("Order");
        }

        public void onClick(final int pos){
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String price_s = price.getText().toString().trim();
                    String desc_s = description.getText().toString().trim();
                    String name_s = name.getText().toString().trim();
                    order.setDesc(desc_s);
                    order.setName(name_s);
                    order.setPrice(price_s);
                    databaseReference.push().setValue(order);
                    Toast.makeText(context, "buyed succesfully", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
